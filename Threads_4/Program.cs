﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Threads_4
{
    class Program
    {
        //this is that thread static variable
        //a copy is given to each thread
        [ThreadStatic]
        public static int static_count = 0;

        //all threads use the same copy
        static int count = 0;

        static void Main(string[] args)
        {
            //two threads with thier own methods 

            Thread t1 = new Thread(new ThreadStart( () =>
            {
                for(int i=0;i<5;i++)
                {
                    //since this is static, static count will only get incremented total 5 times
                    static_count++;
                    //since this is not staic, both threads use the same copy. so, count will get incremented 10 times
                    count++;
                    Console.WriteLine(" Thread 1 - static count is {0} count is {1}", static_count, count);
                    //here I am using the Thread class to get information about the current thread
                    //I can get other stuff as well 
                    Console.WriteLine("Thread id is - " + Thread.CurrentThread.ManagedThreadId);
                    Thread.Sleep(1000);
                }
            }
                ));

            Thread t2 = new Thread(new ThreadStart( () =>
            {
                for (int i = 0; i < 5; i++)
                {
                    static_count++;
                    count++;
                    Console.WriteLine(" Thread 2 - static count is {0} count is {1}", static_count, count);
                    Console.WriteLine("Thread id is - " + Thread.CurrentThread.ManagedThreadId);
                    Thread.Sleep(1000);
                }
            }
                ));

            t1.Start();
            t2.Start();


            //also, using the thread pool
            ThreadPool.QueueUserWorkItem((x)=> {
                Console.WriteLine("This is running from the managed thread");
                Console.WriteLine("The thread name is - {0}", Thread.CurrentThread.ManagedThreadId);
            });
            //lets prevent the console from dissapearing
            Console.ReadLine();
        }
    }
}
